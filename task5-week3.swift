
import Foundation
//task 5
//1. З масиву чисел вивести тільки парні (ex: [1, 2, 12, 5, 7, 88])

var mainArr = [1, 2, 12, 5, 7, 88]
var arrEvenNumbers = [Int]()
for element in mainArr {
    if element % 2==0 {
      arrEvenNumbers.append(element)
    }
}
print (arrEvenNumbers)

//2. Обчислити суму всіх чисел з масиву (ex: [1, 2, 12, 5, 7, 88])
var mainArr2 = [1, 2, 12, 5, 7, 88]
var sumOfmainArr2 = 0
for number in mainArr2 {
    sumOfmainArr2 += number
}
print (sumOfmainArr2)

//3. Конвертувати масив [Strings] у масив [Int] (ex: ["1", "2", "12", "bla", "5", "7", "88"]) -> ([])
var strArr = ["1", "2", "12", "bla", "5", "7", "88"]
let intArr = strArr.compactMap { Int($0) } 
print (intArr)

