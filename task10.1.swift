import Foundation
enum Transmission: CaseIterable {
  case variable, tiptronic, robot, manual
}
struct Car {
  var model: String
  var enginePower: Int
  var transmission: Transmission
}

extension Car: CustomStringConvertible {
  var description: String {
        return "\(self.model) - \(self.enginePower) - \(self.transmission)"
    }
  }

func sortedCars(with cars: [Car]) -> [[Car]] {
    var newArray = [[Car]]()
    
    for transmission in Transmission.allCases {
        let array = cars.filter { 
          car in
          car.transmission.hashValue == transmission.hashValue
        }
        newArray.append(array)
    }
    return newArray
}

let cars: Array = [
    Car(model: "Porsche", enginePower: 120, transmission: .tiptronic),
    Car(model: "Lada", enginePower: 80, transmission: .manual),
    Car(model: "Kia", enginePower: 100, transmission: .manual),
    Car(model: "Toyota", enginePower: 160, transmission: .variable),
    Car(model: "Ford", enginePower: 140, transmission: .robot),
    Car(model: "BMW", enginePower: 150, transmission: .tiptronic),
    Car(model: "Opel", enginePower: 100, transmission: .robot),
    Car(model: "Subaru", enginePower: 120, transmission: .variable),
]

print (sortedCars(with: cars))