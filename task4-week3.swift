import Foundation

import Foundation
enum OtherSolution: Error {
  case d (Double)
  case oneX (Double)
}
var x = 0.0
var d = 0.0
var result = (0.0, 0.0)
func quadraticEquation (_ a: Double, _ b: Double, _ c: Double) throws -> (Double, Double) {
    guard a != 0 else {
      x = -c / b 
      throw OtherSolution.oneX (x)
    }
     d = b * b - 4 * a * c

    if d < 0 {
      throw OtherSolution.d (d)
    } else if d == 0 {
      let x1 = -b / (2 * a)
      let x2 = x1
      return (x1, x2)
    } else {
      let x1 = ( -b + sqrt(d)) / (2 * a);
      let x2 = ( -b - sqrt(d)) / (2 * a)
      return (x1, x2)
    }
}

do {
  result = try quadraticEquation (3, 11.0, 4.0)
} catch OtherSolution.d { 
  print (d)
} catch OtherSolution.oneX {
  print(x)
}
print(result)



