import Foundation
func quadraticEquation(a: Double, b: Double, c: Double) {
    
    let d = b * b - 4 * a * c
    var rootFirst = ""
    var rootSecond = ""
    if d > 0 {
        rootFirst = "\((-b + sqrt(d)) / (a * 2))"
        rootSecond = "\((-b - sqrt(d)) / (a * 2))"
        
    } else if d == 0 {
        rootFirst = "\(-b / (a * 2))"
        rootSecond = rootFirst
        
    } else {
     rootFirst = "noResult"
    }
     print("\(a)a*x^2 + \(b)*x + \(c) = 0, root1 = \(rootFirst), root2 = \(rootSecond)")

}

quadraticEquation(a: 5, b: 18, c: 6)
   

